import { Box } from '@mui/material';
import { Route, Routes } from 'react-router-dom';
import { DrawerCustom } from '../../components';
import { mainRoutes } from '../../routes';

const Main = () => {
    return (
        <Box sx={{ display: "flex" }}>
            <DrawerCustom />
            <Routes>
                {mainRoutes.map((route, index) => (
                    <Route key={index} {...route} element={<route.element />} />
                ))}
            </Routes>
        </Box>
    )
}

export default Main;