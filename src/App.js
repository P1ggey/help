import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { appRoutes } from './routes';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        {appRoutes.map((route, index) => (
          <Route key={index} {...route} element={<route.element />} />
        ))}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
