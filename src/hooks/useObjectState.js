import React from "react";

const useObjectState = (initState) => {
    const [state, setState] = React.useState(initState);

    return {
        state,
        setState
    }
}

export default useObjectState;