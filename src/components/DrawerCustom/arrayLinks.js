import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';

const arrayLinks = [
    {
        text: 'table example',
        icon: InboxIcon,
        url: '/table-example',
    },
    {
        text: 'table second',
        icon: MailIcon,
        url: '/table-second',
    }
]

export default arrayLinks;