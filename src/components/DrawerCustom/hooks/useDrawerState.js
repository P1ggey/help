import useObjectState from "../../../hooks/useObjectState";

const useDrawerState = () => {
    const drawerState = useObjectState(false);

    return {
        drawerState,
    };
}

export default useDrawerState;