import Main from "../pages/Main";
import TableExample from "../pages/Main/Table"
import TableSecond from "../pages/Main/TableSecond"

export const RouteNames = {
    MAIN: '*',
    TABLE_EXAMPLE: '/table-example',
    TABLE_SECOND: '/table-second',
};

export const appRoutes = [
    {path: RouteNames.MAIN, index: true, element: Main},
];

export const mainRoutes = [
    {path: RouteNames.TABLE_EXAMPLE, exact: true, element: TableExample},
    {path: RouteNames.TABLE_SECOND, exact: true, element: TableSecond},
];